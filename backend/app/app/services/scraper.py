import re

import requests
from app.services.itunes_app_scraper.scraper import AppStoreScraper
from app.services.app_store_scraper.app_store import AppStore
from bs4 import BeautifulSoup
from subprocess import PIPE, Popen

scraper = AppStoreScraper()


def search_apps(query):
    ids = scraper.get_app_ids_for_query(query)
    return scraper.get_multiple_app_details(ids)


def get_app_id_from_url(url):
    return url.split('/')[-1].replace('id', '')


def get_app_name_from_url(url):
    return url.split('/')[-2]


def parse_as_html(url):
    try:
        result = requests.get(url)

        if result.status_code != 200:
            return {}

        soup = BeautifulSoup(result.text, 'html.parser')
        # parse subtitle
        subtitle = soup.find("h2", {"class": "app-header__subtitle"})
        if subtitle:
            subtitle = subtitle.text.strip()

        iphone_compatibility = soup.select_one("#ember134 > "
                                               "section.l-content-width.section.section--bordered.section--information"
                                               " > div:nth-child(1) > dl > div:nth-child(4) > dd > dl:nth-child(1) > dd"
                                               )
        if iphone_compatibility:
            iphone_compatibility = iphone_compatibility.text.strip()

        ipod_compatibility = soup.select_one("#ember134 > "
                                             "section.l-content-width.section.section--bordered.section--information"
                                             " > div:nth-child(1) > dl > div:nth-child(4) > dd > dl:nth-child(2) > dd"
                                             )
        if ipod_compatibility:
            ipod_compatibility = ipod_compatibility.text.strip()
        screenshots = []
        images = soup.findAll('source', {"class": "we-artwork__source"})
        for image in images:
            srcset = image.get('srcset')
            if srcset and (srcset.endswith('600x0w.png 2x') or srcset.endswith('600x0w.jpg 2x')):
                screenshots.append(srcset.split(',')[-1].strip().replace(' 2x', ''))
        data = {
            'subtitle': subtitle,
            'iphone_compatibility': iphone_compatibility,
            'ipod_compatibility': ipod_compatibility,
            'big_screenshotUrls': screenshots
        }
        return data
    except:
        return {}


def parse_appstore_url(url):
    app_id = get_app_id_from_url(url)
    app_name = get_app_name_from_url(url)
    country = url.split('/')[-4]
    info = scraper.get_app_details(app_id, country=country)
    review_scraper = AppStore(app_name=app_name, app_id=app_id, country=country)
    review_scraper.review(how_many=20)
    reviews = review_scraper.reviews[:20]
    extra_info = {}
    try:
        p = Popen(f"node node/scraper.js {app_id}", shell=True, stderr=PIPE, stdout=PIPE)
        (out, err) = p.communicate()
        result = out.decode('utf-8')
    except:
        pass
    return {**info, 'reviews': reviews, **parse_as_html(url), **extra_info}
