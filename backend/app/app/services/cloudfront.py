import datetime
import uuid

import boto3
import time
from app.core.config import settings

import logging

from botocore.exceptions import ClientError

cf = boto3.client(
    'cloudfront',
    aws_access_key_id=settings.AWS_SERVER_PUBLIC_KEY,
    aws_secret_access_key=settings.AWS_SERVER_SECRET_KEY,
    region_name='us-east-1',
)

s3_client = boto3.client(
    's3',
    aws_access_key_id=settings.AWS_SERVER_PUBLIC_KEY,
    aws_secret_access_key=settings.AWS_SERVER_SECRET_KEY,
    region_name='us-east-1',
)


def upload_file_to_bucket(file_obj, bucket, folder, object_name=None):
    """Upload a file to an S3 bucket
    :param file_obj: File to upload
    :param bucket: Bucket to upload to
    :param folder: Folder to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = str(uuid.uuid4())

    key = f"{folder}/{object_name}"

    # Upload the file
    try:
        s3_client.upload_fileobj(file_obj, bucket, key, ExtraArgs={'ACL': 'public-read'})
        bucket_location = s3_client.get_bucket_location(Bucket=bucket)
        object_url = "https://s3-{0}.amazonaws.com/{1}/{2}".format(
            bucket_location['LocationConstraint'],
            bucket,
            key)
        return object_url
    except ClientError as e:
        logging.error(e)
        return False
    return True


def create_aws_cf_url():
    distribution_config_response = cf.get_distribution_config(Id='E15NBGGKZAX59D')
    distribution_config = distribution_config_response['DistributionConfig']
    distribution_config['DefaultRootObject'] = 'index.html'
    distribution_config['Origins']['Items'][0]['OriginPath'] = '/template_new/template'
    distribution_config['CallerReference'] = str(int(time.mktime(datetime.datetime.now().timetuple())))
    new_cf = cf.create_distribution(DistributionConfig=distribution_config)
    return new_cf['Distribution']['DomainName']