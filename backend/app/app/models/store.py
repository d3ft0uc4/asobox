from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Numeric
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Store(Base):
    id = Column(Integer, primary_key=True, index=True)
    store_name = Column(String, index=True)

    cloudfront_domain = Column(String, index=True, nullable=True)

    title = Column(String, index=True)
    subtitle = Column(String)
    description = Column(String, nullable=True)
    about = Column(String, nullable=True)
    developer = Column(String)
    store_url = Column(String, index=True, nullable=True)
    size = Column(String, index=True, nullable=True)

    age = Column(Integer, nullable=True)
    category = Column(String, nullable=True)
    image_url = Column(String, index=True, nullable=True)

    owner_id = Column(Integer, ForeignKey("user.id"))
    owner = relationship("User", back_populates="stores")

    comments = relationship("Comment", back_populates="store", cascade="all, delete")
    images = relationship("Image", back_populates="store", cascade="all, delete")
    sessions = relationship("Session", back_populates="store", cascade="all, delete")

    whats_new = Column(String)
    version = Column(String)

    five_star_rating = Column(Integer)
    four_star_rating = Column(Integer)
    three_star_rating = Column(Integer)
    two_star_rating = Column(Integer)
    one_star_rating = Column(Integer)

    overall_rating = Column(Numeric)
    ratings = Column(Integer)

    seller = Column(String, index=True, nullable=True)

    iphone_compatibility = Column(String)
    ipod_compatibility = Column(String)

    languages = Column(String)

    copyright = Column(String)

    ym = Column(String)
    click_link = Column(String)


class Comment(Base):
    id = Column(Integer, primary_key=True, index=True)

    store_id = Column(Integer, ForeignKey("store.id"))
    store = relationship("Store", back_populates="comments")

    user = Column(String)
    title = Column(String)
    comment = Column(String)
    date = Column(DateTime)
    order = Column(Integer)

    developer_response = Column(String)

    __mapper_args__ = {
        "order_by": order
    }


class Image(Base):
    id = Column(Integer, primary_key=True, index=True)

    store_id = Column(Integer, ForeignKey("store.id"))
    store = relationship("Store", back_populates="images")

    type = Column(String)
    link = Column(String)
    order = Column(Integer)

    __mapper_args__ = {
        "order_by": order
    }
