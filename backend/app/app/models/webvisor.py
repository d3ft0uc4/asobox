import uuid

from app.db.base_class import Base
from sqlalchemy import Column, ForeignKey, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship


class Session(Base):
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    store_id = Column(Integer, ForeignKey("store.id"))
    store = relationship("Store", back_populates="sessions")
    events = relationship("Event", back_populates="session")


class Event(Base):
    id = Column(Integer, primary_key=True, index=True)

    session = relationship("Session", back_populates="events")
    session_id = Column(UUID(as_uuid=True), ForeignKey("session.id"))