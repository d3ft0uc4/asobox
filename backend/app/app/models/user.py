from app.db.base_class import Base
from sqlalchemy import Boolean, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship


class User(Base):
    id = Column(Integer, primary_key=True, index=True)
    full_name = Column(String, index=True)
    email = Column(String, unique=True, index=True, nullable=False)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    stores = relationship("Store", back_populates="owner")

    workspace_id = Column(Integer, ForeignKey("workspace.id"))
    workspace = relationship("Workspace", back_populates="users")


class Workspace(Base):
    id = Column(Integer, primary_key=True, index=True)
    users = relationship("User", back_populates="workspace")