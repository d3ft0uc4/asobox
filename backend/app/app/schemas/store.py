from typing import Optional, List

from app.models import Image, Comment
from pydantic import BaseModel


# Shared properties
class StoreBase(BaseModel):
    store_name: Optional[str]
    title: Optional[str]
    description: Optional[str]
    about: Optional[str]
    store_url: Optional[str]
    size: Optional[str]
    seller: Optional[str]
    age: Optional[int]
    category: Optional[str]
    image_url: Optional[str]
    comments: Optional[List[dict]]
    subtitle: Optional[str]
    overall_rating: Optional[float]
    ratings: Optional[int]
    version: Optional[str]
    whats_new: Optional[str]
    images: Optional[list]
    comments: Optional[list]
    developer: Optional[str]
    ym: Optional[str]
    click_link: Optional[str]


# Properties to receive on item creation
class StoreCreate(StoreBase):
    existing_url: Optional[str]


# Properties to receive on item update
class StoreUpdate(StoreBase):
    pass


# Properties to return to client
class Store(StoreBase):
    id: int
    images: list
    comments: list
    cloudfront_domain: Optional[str]

    class Config:
        orm_mode = True
