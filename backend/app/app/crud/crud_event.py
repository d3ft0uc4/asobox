from app.crud.base import CRUDBase
from app.models.store import Image


class CRUDEvent(CRUDBase):
    pass


event = CRUDEvent(Image)
