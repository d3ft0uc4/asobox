from app.crud.base import CRUDBase
from app.models.store import Image


class CRUDImage(CRUDBase):
    pass


image = CRUDImage(Image)
