from typing import List, Optional, Any

from app.crud.base import CRUDBase
from app.crud.crud_image import image as crud_image
from app.models.store import Store, Comment, Image
from app.schemas.store import StoreCreate
from app.services.cloudfront import create_aws_cf_url
from app.services.scraper import parse_appstore_url
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.orm.session import make_transient


class CRUDStore(CRUDBase):
    def create_with_owner(
            self, db: Session, *, obj_in: StoreCreate, owner_id: int
    ) -> Store:

        obj_in_data = jsonable_encoder(obj_in)
        existing_url = obj_in_data.pop('existing_url', None)

        if existing_url:
            info = parse_appstore_url(existing_url)

            obj_in_data['title'] = info['trackName']
            obj_in_data['store_name'] = info['trackName']
            obj_in_data['about'] = info['description']

            obj_in_data['store_url'] = existing_url

            obj_in_data['size'] = round(float(info['fileSizeBytes']) / (1000 ** 2), 1)
            obj_in_data['seller'] = info['sellerName']

            obj_in_data['age'] = info['trackContentRating'].replace('+', '')
            obj_in_data['category'] = info['primaryGenreName']

            obj_in_data['image_url'] = info['artworkUrl512']

            obj_in_data['developer'] = info['artistName']

            obj_in_data['subtitle'] = info.get('subtitle')

            obj_in_data['ratings'] = info['userRatingCount']

            obj_in_data['overall_rating'] = info['averageUserRating']

            obj_in_data['version'] = info['version']

            obj_in_data['whats_new'] = info['releaseNotes']

            obj_in_data['languages'] = info.get('languages')
            obj_in_data['iphone_compatibility'] = info.get('iphone_compatibility')
            obj_in_data['ipod_compatibility'] = info.get('ipod_compatibility')

            obj_in_data['comments'] = [{
                'user': comment['userName'],
                'title': comment['title'],
                'comment': comment['review'],
                'date': comment['date'],
                'order': idx
            } for idx, comment in enumerate(info['reviews'])]

            obj_in_data['images'] = [{
                'link': link,
                'order': idx,
                'type': 'iphone_8'
            } for idx, link in enumerate(info['screenshotUrls'].split(','))]

            obj_in_data['images'].extend([{
                'link': link,
                'order': idx,
                'type': 'iphone_x'
            } for idx, link in enumerate(info.get('big_screenshotUrls', []))])

        obj_in_data['comments'] = [{
            **comment,
            'order': idx
        } for idx, comment in enumerate(obj_in_data['comments'])]

        obj_in_data['images'] = [{
            **image,
            'order': idx
        } for idx, image in enumerate(obj_in_data['images'])]

        comments = obj_in_data.pop('comments', []) or []
        images = obj_in_data.pop('images', []) or []
        db_obj = self.model(**obj_in_data, owner_id=owner_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        for c in comments:
            c.pop('id', None)
            comment_obj = Comment(**c, store_id=db_obj.id)
            db.add(comment_obj)
            db.commit()
        for i in images:
            i.pop('id', None)
            image_obj = Image(**i, store_id=db_obj.id)
            db.add(image_obj)
            db.commit()
        cloudfront_domain = create_aws_cf_url()
        setattr(db_obj, 'cloudfront_domain', cloudfront_domain)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def get_multi_by_owner(
            self, db: Session, *, owner_id: int, skip: int = 0, limit: int = 100
    ) -> List[Store]:
        return (db.query(self.model)
                .filter(Store.owner_id == owner_id)
                .offset(skip)
                .limit(limit)
                .all())

    def update(
            self,
            *args,
            **kwargs
    ) -> Store:

        db = kwargs.get('db')
        images = kwargs.get('obj_in').images
        comments = kwargs.get('obj_in').comments

        db_obj = super().update(*args, **kwargs)

        for image in db_obj.images:
            db.delete(image)

        for comment in db_obj.comments:
            db.delete(comment)

        for idx, c in enumerate(comments):
            c.pop('id', None)
            c.pop('store_id', None)
            c['order'] = idx
            comment_obj = Comment(**c, store_id=db_obj.id)
            db.add(comment_obj)

        for idx, i in enumerate(images):
            i.pop('id', None)
            i.pop('store_id', None)
            i['order'] = idx
            image_obj = Image(**i, store_id=db_obj.id)
            db.add(image_obj)

        db.commit()

        return db_obj

    def get_by_domain(self, db: Session, cloudfront_domain: Any) -> Optional[Store]:
        return db.query(self.model).filter(self.model.cloudfront_domain == cloudfront_domain).first()

    def duplicate(self, db: Session, db_obj: Store) -> Store:
        images = db_obj.images
        comments = db_obj.comments
        setattr(db_obj, 'id', None)
        setattr(db_obj, 'store_name', db_obj.store_name + ' (Copy)')
        db.expunge(db_obj)
        make_transient(db_obj)
        cloudfront_domain = create_aws_cf_url()
        setattr(db_obj, 'cloudfront_domain', cloudfront_domain)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        for image in images:
            setattr(image, 'id', None)
            setattr(image, 'store', db_obj)
            db.expunge(image)
            make_transient(image)
            db.add(image)
            db.commit()
            db.refresh(image)

        for comment in comments:
            setattr(comment, 'id', None)
            setattr(comment, 'store', db_obj)
            db.expunge(comment)
            make_transient(comment)
            db.add(comment)
            db.commit()
            db.refresh(comment)
        db.refresh(db_obj)
        return db_obj


store = CRUDStore(Store)
