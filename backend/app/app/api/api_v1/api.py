from fastapi import APIRouter

from app.api.api_v1.endpoints \
    import login, users, utils, stores, events, search, images, comments

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(search.router, prefix="/search", tags=["search"])
api_router.include_router(stores.router, prefix="/stores", tags=["stores"])
api_router.include_router(images.router, prefix="/images", tags=["images"])
api_router.include_router(comments.router, prefix="/comments", tags=["comments"])
