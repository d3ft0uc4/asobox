from typing import Any, List

from app import models
from app.api import deps
from app.services.scraper import search_apps
from fastapi import APIRouter, Depends, HTTPException

router = APIRouter()


@router.get("/", response_model=List[dict])
def search_app_store(
        query: str,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    if not current_user:
        raise HTTPException(status_code=401, detail="Unauthorized")
    return search_apps(query)



@router.post("/", response_model=dict)
def parse_app_store(
        query: str,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    if not current_user:
        raise HTTPException(status_code=401, detail="Unauthorized")
    return search_apps(query)
