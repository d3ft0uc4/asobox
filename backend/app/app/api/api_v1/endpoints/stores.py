from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/", response_model=List[schemas.Store])
def read_stores(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve stores.
    """
    if crud.user.is_superuser(current_user):
        stores = crud.store.get_multi(db, skip=skip, limit=limit)
    else:
        stores = crud.store.get_multi_by_owner(
            db=db, owner_id=current_user.id, skip=skip, limit=limit
        )
    return stores


@router.post("/", response_model=schemas.Store)
def create_store(
        *,
        db: Session = Depends(deps.get_db),
        store_in: schemas.StoreCreate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new store.
    """
    store = crud.store.create_with_owner(db=db, obj_in=store_in, owner_id=current_user.id)
    return store


@router.put("/{id}", response_model=schemas.Store)
def update_store(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        store_in: schemas.StoreUpdate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an store.
    """
    store = crud.store.get(db=db, id=id)
    if not store:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (store.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    store = crud.store.update(db=db, db_obj=store, obj_in=store_in)

    return store


@router.get("/domain/{domain}", response_model=schemas.Store)
def read_store_by_domain(
        *,
        db: Session = Depends(deps.get_db),
        domain: str,
) -> Any:
    """
    Get store by cloudfront_domain.
    """
    store = crud.store.get_by_domain(db=db, cloudfront_domain=domain)
    if not store:
        raise HTTPException(status_code=404, detail="Item not found")
    return store


@router.get("/{id}", response_model=schemas.Store)
def read_store(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get store by ID.
    """
    store = crud.store.get(db=db, id=id)
    if not store:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (store.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return store


@router.delete("/{id}", response_model=schemas.Item)
def delete_store(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an store.
    """
    store = crud.store.get(db=db, id=id)
    if not store:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (store.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    store = crud.store.remove(db=db, id=id)
    return store


@router.get("/duplicate/{id}", response_model=schemas.Item)
def duplicate_store(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an store.
    """
    store = crud.store.get(db=db, id=id)
    if not store:
        raise HTTPException(status_code=404, detail="Item not found")
    if not crud.user.is_superuser(current_user) and (store.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    store = crud.store.duplicate(db=db, db_obj=store)
    return store
