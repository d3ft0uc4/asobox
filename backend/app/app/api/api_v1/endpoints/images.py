from typing import Any

from app import crud, models
from app.api import deps
from app.services.cloudfront import upload_file_to_bucket
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse

router = APIRouter()


@router.get("/")
def read_images(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    images = crud.image.get_multi(db, skip=skip, limit=limit)
    return images


@router.post("/")
def create_image(
        *,
        db: Session = Depends(deps.get_db),
        image_in: dict,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new image.
    """
    if not crud.store.get(db=db, id=image_in.get('store_id')):
        raise HTTPException(status_code=404, detail="Store not found")
    image = crud.image.create(db=db, obj_in=image_in)
    return image


@router.put("/{id}")
def update_image(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        image_in: Any,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an image.
    """
    if not crud.store.get(db=db, id=image_in.get('store_id')):
        raise HTTPException(status_code=404, detail="Store not found")
    image = crud.image.get(db=db, id=id)
    if not image:
        raise HTTPException(status_code=404, detail="Item not found")
    image = crud.image.update(db=db, db_obj=image, obj_in=image_in)
    return image


@router.get("/{id}")
def read_image(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get image by ID.
    """
    image = crud.image.get(db=db, id=id)
    if not image:
        raise HTTPException(status_code=404, detail="Item not found")
    return image


@router.delete("/{id}")
def delete_image(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an image.
    """
    image = crud.image.get(db=db, id=id)
    if not image:
        raise HTTPException(status_code=404, detail="Item not found")
    image = crud.image.remove(db=db, id=id)
    return image


@router.post("/upload")
async def upload(request: Request):
    form = await request.form()
    image = form.get('file') or form.get('image')
    upload_obj = upload_file_to_bucket(file_obj=image.file,
                                       bucket='astore-1',
                                       folder='images',
                                       object_name=image.filename
                                       )

    if upload_obj:
        return JSONResponse(content={'url': upload_obj},
                            status_code=status.HTTP_201_CREATED)
    else:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                            detail="File could not be uploaded")