from typing import Any, List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.get("/")
def read_comments(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 100,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    comments = crud.comment.get_multi(db, skip=skip, limit=limit)
    return comments


@router.post("/")
def create_comment(
        *,
        db: Session = Depends(deps.get_db),
        comment_in: dict,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new comment.
    """
    store = crud.store.get(db=db, id=comment_in.get('id'))
    comment = crud.comment.create(db=db, obj_in=comment_in)
    return comment


@router.put("/{id}")
def update_comment(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        comment_in: dict,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an comment.
    """
    comment = crud.comment.get(db=db, id=id)
    if not comment:
        raise HTTPException(status_code=404, detail="Item not found")
    comment = crud.comment.update(db=db, db_obj=comment, obj_in=comment_in)
    return comment


@router.get("/{id}")
def read_comment(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get comment by ID.
    """
    comment = crud.comment.get(db=db, id=id)
    if not comment:
        raise HTTPException(status_code=404, detail="Item not found")
    return comment


@router.delete("/{id}", response_model=schemas.Item)
def delete_comment(
        *,
        db: Session = Depends(deps.get_db),
        id: int,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an comment.
    """
    comment = crud.comment.get(db=db, id=id)
    if not comment:
        raise HTTPException(status_code=404, detail="Item not found")
    comment = crud.comment.remove(db=db, id=id)
    return comment
