import request from '@/utils/request';

export async function queryList(params) {
  return request('/stores', {
    params,
  });
}
export async function removeFakeList(params) {
  const { id } = params;
  return request(`/stores/${id}`, {
    method: 'DELETE',
    params: {
    },
    data: { },
  });
}
export async function addFakeList(params) {
  return request('/stores', {
    method: 'POST',
    data: params,
  });
}
export async function updateFakeList(params) {
  const { id , ...restParams } = params;
  return request(`/stores/${id}`, {
    method: 'PUT',
    params: {
    },
    data: { ...restParams},
  });
}
