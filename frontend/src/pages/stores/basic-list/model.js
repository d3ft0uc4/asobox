import { addFakeList, queryList, removeFakeList, updateFakeList } from './service';
const Model = {
  namespace: 'storeAndbasicList',
  state: {
    list: [],
  },
  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryList, payload);
      yield put({
        type: 'queryList',
        payload: Array.isArray(response) ? response : [],
      });
    },

    *appendFetch({ payload }, { call, put }) {
      const response = yield call(queryList, payload);
      yield put({
        type: 'appendList',
        payload: Array.isArray(response) ? response : [],
      });
    },

    *submit({ payload }, { call, put }) {
      let callback;

      if (payload.id) {
        callback = Object.keys(payload).length === 1 ? removeFakeList : updateFakeList;
      } else {
        callback = addFakeList;
      }

      yield call(callback, payload); // post

      const response = yield call(queryList);

      yield put({
        type: 'queryList',
        payload: Array.isArray(response) ? response : [],
      });
    },
  },
  reducers: {
    queryList(state, action) {
      return { ...state, list: Array.isArray(action.payload) ? action.payload : [] };
    },

    // appendList(
    //   state = {
    //     list: [],
    //   },
    //   action,
    // ) {
    //   return { ...state, list: state.list.concat(action.payload) };
    // },
  },
};
export default Model;
