import React, {useEffect, useState} from 'react';
import moment from 'moment';
import {
  Modal,
  Result,
  Button, Form, Upload, Input, Radio, Space, DatePicker
} from 'antd';

import { PlusOutlined, MinusCircleOutlined} from '@ant-design/icons';
import styles from '../style.less';
import {BACKEND_URI} from "@/utils/utils";
import MyUpload from "@/pages/stores/basic-list/components/MyUpload";

const {TextArea} = Input;
const formLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 13,
  },
};

const OperationModal = (props) => {

  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const uploadButton = (
    <div>
      <PlusOutlined/>
      <div style={{marginTop: 8}}>Upload</div>
    </div>
  );
  const [radio, setRadio] = useState('scratch')
  const [asURL, setAsURL] = useState({
    value: '',
  });
  const [form] = Form.useForm();
  const {done, visible, current, onDone, onCancel, onSubmit} = props;
  useEffect(() => {
    setRadio('scratch')
    if (current) {
      form.setFieldsValue({
        ...current
      });
    } else {
      form.resetFields();
    }
  }, [props.current]);

  const handleSubmit = () => {
    if (!form) return;
    form.validateFields().then(() => {
      form.submit();
    }).catch(() => {
    })
  };

  const handleFinish = (values) => {
    if (onSubmit) {
      onSubmit(values);
    }
  };

  const modalFooter = done
    ? {
      footer: null,
      onCancel: onDone,
    }
    : {
      okText: 'Save',
      onOk: handleSubmit,
      onCancel,
    };

  const getModalContent = () => {
    if (done) {
      return (
        <Result
          status="success"
          title="Success"
          subTitle=""
          extra={
            <Button type="primary" onClick={onDone}>
              Close
            </Button>
          }
          className={styles.formResult}
        />
      );
    }

    const onRadioChange = e => {
      setRadio(e.target.value);
    };

    const validateAsURL = (url) => {
      if (url === 11) {
        return {
          validateStatus: 'success',
          errorMsg: null,
        };
      }

      return {
        validateStatus: 'error',
        errorMsg: 'The prime between 8 and 12 is 11!',
      };
    }
    const asUrlChange = (value) => {
      setAsURL({...validateAsURL(value), value});
    };
    return (
      <Form {...formLayout} form={form} onFinish={handleFinish}>
        <Form.Item name="id" hidden/>
        <Form.Item label="Choose creating option">
          <Radio.Group onChange={onRadioChange} defaultValue="scratch">
            <Radio.Button value="scratch">Create from scratch</Radio.Button>
            <Radio.Button value="url">Parse from URL</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item
          name="store_name"
          label="Store name"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input placeholder="Store name"/>
        </Form.Item>
        {radio === 'url' && <>
          <Form.Item
            name="existing_url"
            label="Application URL"
            validateStatus={asURL.validateStatus}
            help={asURL.errorMsg}
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input placeholder="Type URL here..." value={asURL.value} onChange={asUrlChange}/>
          </Form.Item>
        </>}
        {radio === 'scratch' &&
        <>
          <Form.Item
            name="cloudfront_domain"
            label="Domain"
            rules={[]}
          >
            <Input placeholder="Domain" readOnly={true}/>
          </Form.Item>
          <Form.Item
            name="image_65"
            label="Size 6.5"
            // valuePropName="fileList"
            // getValueFromEvent={normFile}
          >
            <MyUpload
              name="image"
              accept="image/*"
              action={`${BACKEND_URI}/images/upload`}
              listType="picture-card"
              showUploadList={true}
              beforeUpload={false}
            >
              {uploadButton}
            </MyUpload>
          </Form.Item>
          <div id="image_55"/>
          <Form.Item
            name="image_55"
            label="Size 5.5"
            valuePropName="fileList"
            getValueFromEvent={normFile}
          >
            <Upload
              name="image"
              accept="image/*"
              action={`${BACKEND_URI}/images/upload`}
              listType="picture-card"
              className="avatar-uploader"
              showUploadList={true}
              beforeUpload={false}
            >
              {uploadButton}
            </Upload>
          </Form.Item>

          <Form.Item
            name="title"
            label="Title"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input placeholder="Title"/>
          </Form.Item>

          <Form.Item
            name="description"
            label="Subtitle"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder="Subtitle"/>
          </Form.Item>

          <Form.Item
            name="about"
            label="About"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <TextArea placeholder="About"/>
          </Form.Item>

          <Form.Item
            name="store_url"
            label="Main link"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input placeholder="Main link"/>
          </Form.Item>

          <Form.Item
            name="seller"
            label="Seller"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>

          <Form.Item
            name="size"
            label="Size"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>

          <Form.Item
            name="age"
            label="Age"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>

          <Form.Item
            name="category"
            label="Category"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>

          <Form.Item
            name="metrika"
            label="Yandex metrika"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>

          <Form.List name="comments" label="Comments">
            {(fields, {add, remove}) => (
              <>
                {fields.map(field => (
                  <Space key={field.key} align="baseline">
                    <Form.Item
                      noStyle
                      shouldUpdate={(prevValues, curValues) =>
                        prevValues.comments !== curValues.comments
                      }
                    >
                      {() => <>
                        <Form.Item
                          {...field}
                          key={`-${field.key}`}
                          label="Title"
                          name={[field.name, 'title']}
                          fieldKey={[field.fieldKey, 'title']}
                          rules={[{required: true, message: 'Missing title'}]}
                        >
                          <Input/>
                        </Form.Item>
                        <Form.Item
                          key={`--${field.key}`}
                          {...field}
                          label="Username"
                          name={[field.name, 'user']}
                          fieldKey={[field.fieldKey, 'user']}
                          rules={[{required: true, message: 'Missing username'}]}
                        >
                          <Input/>
                        </Form.Item>
                        {/* <Form.Item */}
                        {/*  {...field} */}
                        {/*  label="Date" */}
                        {/*  name={[field.name, 'date']} */}
                        {/*  fieldKey={[field.fieldKey, 'date']} */}
                        {/*  rules={[{ required: true, message: 'Missing date' }]} */}
                        {/* > */}
                        {/*  <DatePicker /> */}
                        {/* </Form.Item> */}
                        <Form.Item
                          {...field}
                          label="Comment text"
                          name={[field.name, 'comment']}
                          fieldKey={[field.fieldKey, 'comment']}
                          rules={[{required: true, message: 'Missing text'}]}
                        >
                          <TextArea/>
                        </Form.Item>
                      </>
                      }
                    </Form.Item>

                    <MinusCircleOutlined onClick={() => remove(field.name)}/>
                  </Space>
                ))}

                <Form.Item>
                  <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>
                    Add comments
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
          <Form.Item
            name="click_js"
            label="Click js"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>
          <Form.Item
            name="view_js"
            label="View js"
            rules={[
              {
                required: false,
              },
            ]}
          >
            <Input placeholder=""/>
          </Form.Item>
        </>
        }
      </Form>
    );
  };

  return (
    <Modal
      title={done ? null : `${current ? 'Edit store' : 'Create store'}`}
      className={styles.standardListForm}
      width={1024}
      bodyStyle={
        done
          ? {
            padding: '72px 0',
          }
          : {
            padding: '28px 0 0',
          }
      }
      destroyOnClose
      visible={visible}
      {...modalFooter}
    >
      {getModalContent()}
    </Modal>
  );
};

export default OperationModal;
