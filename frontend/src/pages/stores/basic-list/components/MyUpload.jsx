import React from 'react';
import { Upload, Form } from 'antd';

class MyUpload extends React.Component {
  state = {
    fileList: []
  };

  handleChange = info => {

    let fileList = [...info.fileList];
    // 2. Read from response and show file link
    fileList = fileList.map(file => {
      if (file.response && file.response.url) {
        // Component will show file.url as link
        file.url = file.response.url;
        console.log(file.url)
      }
     return file
    });
    this.setState({ fileList });
  };

  render() {
    return (
      <Upload {...this.props}  onChange={this.handleChange} fileList={this.state.fileList}>
        {this.props.children}
      </Upload>
    );
  }
}

export default MyUpload;
